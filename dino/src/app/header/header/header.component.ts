import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // openMenu() {
  //   document.getElementById("myMenu").style.height = "100%";
  // }
  // closeMenu() {
  //   document.getElementById("myMenu").style.height = "0%";
  // } 

  openMenu() {
    document.getElementById("myMenu").style.width = "220px";
    document.getElementById("main").style.marginRight = "220px";
  }
  
  closeMenu() {
    document.getElementById("myMenu").style.width = "0";
    document.getElementById("main").style.marginRight= "0";
  }

  constructor() { }

  ngOnInit(): void {
  }

}
